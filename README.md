# Overview 
- The purpose of this research dataset is to provide a formatted version of overdose data for data mining.
- This data was derived from the [Allegheny County Fatal Accidental Overdose Data Set](https://catalog.data.gov/dataset/allegheny-county-fatal-accidental-overdoses). This data set was originally published by Allegheny County / City of Pittsburgh / Western PA Regional Data Center.
- This data has been formatted in (*.arff) format to be used with the [WEKA](https://www.cs.waikato.ac.nz/ml/weka/) toolkit.

# Formatting Notes
- Used WEKA to convert from CSV to ARFF.
- Used WEKA to append 2008 through 2017 data to one another. 2017 data was the latest and last updated in June. This WEKA command didn't play nice unless I only used two files at a time: `java weka.core.Instances append D:/overdose/crimelabaccidentaldrugdeathsextract2008.arff D:/overdose/crimelabaccidentaldrugdeathsextract2009.arff > D:/overdose/crimelabaccidentaldrugdeathsextract-timeseries.arff`.
- Broke out the seven drug columns into one column and removed duplicates, in an effort to make them binary values per row for association.
- Ended up with columns similar to this for each drug: `=IF(OR(ISNUMBER(SEARCH("Alcohol",$H1)), ISNUMBER(SEARCH("Alcohol",$I1)), ISNUMBER(SEARCH("Alcohol",$J1)), ISNUMBER(SEARCH("Alcohol",$K1)), ISNUMBER(SEARCH("Alcohol",$L1)), ISNUMBER(SEARCH("Alcohol",$M1)), ISNUMBER(SEARCH("Alcohol",$N1))), T, ?)`
- Removed columns MO and Accident(s) since they all contain the same value. Removed time since I didn't find it relevant for intended outcomes.
- Adjusted some zip codes based on similarities in the two values, and removed some four digit zip codes I couldn't figure out.
- Provided WEKA outputs.

# Contact
- [Email me](mailto:blayzestefaniak@u.northwestern.edu) if you have any questions!