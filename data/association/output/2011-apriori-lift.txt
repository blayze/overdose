=== Run information ===

Scheme:       weka.associations.Apriori -I -N 100000 -T 1 -C 0.6 -D 0.05 -U 1.0 -M 0.1 -S -1.0 -c -1
Relation:     crimelabaccidentaldrugdeathsextract
Instances:    262
Attributes:   60
              Acetaminophen
              Alcohol
              Alprazolam
              Amitriptyline
              Amphetamines
              Benzodiazepines
              Benzoylecgonine
              Bupropion
              Butalbital
              Carbamazepine
              Carbon Monoxide
              Carisoprodol
              Chlordiazepoxide
              Chlorpheniramine
              Citalopram
              Clonazepam
              Cocaine
              Codeine
              Cyclobenzaprine
              Dextromethorphan
              Diazepam
              Diphenhydramine
              Doxepin
              Ethylene Glycol
              Fentanyl
              Fluoxetine
              Gama-Hydroxybutyric Acid
              Glucophage
              Heroin
              Hydrocodone
              Hydromorphone
              Lamotrigine
              Mephobarbital
              Meprobamate
              Methadone
              Methamphetamine
              Midazolam
              Mirtazapine
              Morphine
              Nordiazepam
              Norpropoxyphene
              Opiates
              Oxycodone
              Oxymorphone
              Paroxetine
              Phencyclidine
              Phenobarbital
              Promethazine
              Propoxyphene
              Quetiapine
              Sertraline
              Temazepam
              Topiramate
              Tramadol
              Trazodone
              Valproic Acid
              Venlafaxine
              Verapamil
              Zolpidem
              Zopiclone
=== Associator model (full training set) ===


Apriori
=======

Minimum support: 0.1 (26 instances)
Minimum metric <lift>: 0.6
Number of cycles performed: 18

Generated sets of large itemsets:

Size of set of large itemsets L(1): 7

Large Itemsets L(1):
Alcohol=T 61
Alprazolam=T 42
Cocaine=T 83
Diazepam=T 30
Heroin=T 95
Methadone=T 26
Oxycodone=T 51

Size of set of large itemsets L(2): 1

Large Itemsets L(2):
Cocaine=T Heroin=T 30

Best rules found:

     1. Cocaine=T 83 ==> Heroin=T 30    conf:(0.36) < lift:(1)> lev:(-0) [0] conv:(0.98)
     2. Heroin=T 95 ==> Cocaine=T 30    conf:(0.32) < lift:(1)> lev:(-0) [0] conv:(0.98)


