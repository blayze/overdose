=== Run information ===

Scheme:       weka.associations.Apriori -I -N 100000 -T 1 -C 0.6 -D 0.05 -U 1.0 -M 0.1 -S -1.0 -c -1
Relation:     crimelabaccidentaldrugdeathsextract
Instances:    385
Attributes:   60
              Acetaminophen
              Alcohol
              Alprazolam
              Amitriptyline
              Amphetamines
              Benzodiazepines
              Benzoylecgonine
              Bupropion
              Butalbital
              Carbamazepine
              Carbon Monoxide
              Carisoprodol
              Chlordiazepoxide
              Chlorpheniramine
              Citalopram
              Clonazepam
              Cocaine
              Codeine
              Cyclobenzaprine
              Dextromethorphan
              Diazepam
              Diphenhydramine
              Doxepin
              Ethylene Glycol
              Fentanyl
              Fluoxetine
              Gama-Hydroxybutyric Acid
              Glucophage
              Heroin
              Hydrocodone
              Hydromorphone
              Lamotrigine
              Mephobarbital
              Meprobamate
              Methadone
              Methamphetamine
              Midazolam
              Mirtazapine
              Morphine
              Nordiazepam
              Norpropoxyphene
              Opiates
              Oxycodone
              Oxymorphone
              Paroxetine
              Phencyclidine
              Phenobarbital
              Promethazine
              Propoxyphene
              Quetiapine
              Sertraline
              Temazepam
              Topiramate
              Tramadol
              Trazodone
              Valproic Acid
              Venlafaxine
              Verapamil
              Zolpidem
              Zopiclone
=== Associator model (full training set) ===


Apriori
=======

Minimum support: 0.1 (39 instances)
Minimum metric <lift>: 0.6
Number of cycles performed: 18

Generated sets of large itemsets:

Size of set of large itemsets L(1): 5

Large Itemsets L(1):
Alcohol=T 73
Alprazolam=T 54
Cocaine=T 126
Fentanyl=T 321
Heroin=T 136

Size of set of large itemsets L(2): 5

Large Itemsets L(2):
Alcohol=T Fentanyl=T 58
Alprazolam=T Fentanyl=T 43
Cocaine=T Fentanyl=T 104
Cocaine=T Heroin=T 42
Fentanyl=T Heroin=T 120

Best rules found:

     1. Fentanyl=T 321 ==> Heroin=T 120    conf:(0.37) < lift:(1.06)> lev:(0.02) [6] conv:(1.03)
     2. Heroin=T 136 ==> Fentanyl=T 120    conf:(0.88) < lift:(1.06)> lev:(0.02) [6] conv:(1.33)
     3. Cocaine=T 126 ==> Fentanyl=T 104    conf:(0.83) < lift:(0.99)> lev:(-0) [-1] conv:(0.91)
     4. Fentanyl=T 321 ==> Cocaine=T 104    conf:(0.32) < lift:(0.99)> lev:(-0) [-1] conv:(0.99)
     5. Alprazolam=T 54 ==> Fentanyl=T 43    conf:(0.8) < lift:(0.96)> lev:(-0.01) [-2] conv:(0.75)
     6. Fentanyl=T 321 ==> Alprazolam=T 43    conf:(0.13) < lift:(0.96)> lev:(-0.01) [-2] conv:(0.99)
     7. Alcohol=T 73 ==> Fentanyl=T 58    conf:(0.79) < lift:(0.95)> lev:(-0.01) [-2] conv:(0.76)
     8. Fentanyl=T 321 ==> Alcohol=T 58    conf:(0.18) < lift:(0.95)> lev:(-0.01) [-2] conv:(0.99)
     9. Cocaine=T 126 ==> Heroin=T 42    conf:(0.33) < lift:(0.94)> lev:(-0.01) [-2] conv:(0.96)
    10. Heroin=T 136 ==> Cocaine=T 42    conf:(0.31) < lift:(0.94)> lev:(-0.01) [-2] conv:(0.96)


