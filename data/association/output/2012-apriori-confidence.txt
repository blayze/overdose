=== Run information ===

Scheme:       weka.associations.Apriori -I -N 100000 -T 0 -C 0.9 -D 0.05 -U 1.0 -M 0.1 -S -1.0 -c -1
Relation:     crimelabaccidentaldrugdeathsextract
Instances:    290
Attributes:   60
              Acetaminophen
              Alcohol
              Alprazolam
              Amitriptyline
              Amphetamines
              Benzodiazepines
              Benzoylecgonine
              Bupropion
              Butalbital
              Carbamazepine
              Carbon Monoxide
              Carisoprodol
              Chlordiazepoxide
              Chlorpheniramine
              Citalopram
              Clonazepam
              Cocaine
              Codeine
              Cyclobenzaprine
              Dextromethorphan
              Diazepam
              Diphenhydramine
              Doxepin
              Ethylene Glycol
              Fentanyl
              Fluoxetine
              Gama-Hydroxybutyric Acid
              Glucophage
              Heroin
              Hydrocodone
              Hydromorphone
              Lamotrigine
              Mephobarbital
              Meprobamate
              Methadone
              Methamphetamine
              Midazolam
              Mirtazapine
              Morphine
              Nordiazepam
              Norpropoxyphene
              Opiates
              Oxycodone
              Oxymorphone
              Paroxetine
              Phencyclidine
              Phenobarbital
              Promethazine
              Propoxyphene
              Quetiapine
              Sertraline
              Temazepam
              Topiramate
              Tramadol
              Trazodone
              Valproic Acid
              Venlafaxine
              Verapamil
              Zolpidem
              Zopiclone
=== Associator model (full training set) ===


Apriori
=======

Minimum support: 0.1 (29 instances)
Minimum metric <confidence>: 0.9
Number of cycles performed: 18

Generated sets of large itemsets:

Size of set of large itemsets L(1): 6

Large Itemsets L(1):
Alcohol=T 73
Alprazolam=T 36
Cocaine=T 68
Heroin=T 140
Morphine=T 31
Oxycodone=T 31

Size of set of large itemsets L(2): 2

Large Itemsets L(2):
Alcohol=T Heroin=T 31
Cocaine=T Heroin=T 29

Best rules found:


