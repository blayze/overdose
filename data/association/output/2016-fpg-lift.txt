=== Run information ===

Scheme:       weka.associations.FPGrowth -P 2 -I -1 -N 10 -T 1 -C 0.6 -D 0.05 -U 1.0 -M 0.1
Relation:     crimelabaccidentaldrugdeathsextract
Instances:    617
Attributes:   60
              Acetaminophen
              Alcohol
              Alprazolam
              Amitriptyline
              Amphetamines
              Benzodiazepines
              Benzoylecgonine
              Bupropion
              Butalbital
              Carbamazepine
              Carbon Monoxide
              Carisoprodol
              Chlordiazepoxide
              Chlorpheniramine
              Citalopram
              Clonazepam
              Cocaine
              Codeine
              Cyclobenzaprine
              Dextromethorphan
              Diazepam
              Diphenhydramine
              Doxepin
              Ethylene Glycol
              Fentanyl
              Fluoxetine
              Gama-Hydroxybutyric Acid
              Glucophage
              Heroin
              Hydrocodone
              Hydromorphone
              Lamotrigine
              Mephobarbital
              Meprobamate
              Methadone
              Methamphetamine
              Midazolam
              Mirtazapine
              Morphine
              Nordiazepam
              Norpropoxyphene
              Opiates
              Oxycodone
              Oxymorphone
              Paroxetine
              Phencyclidine
              Phenobarbital
              Promethazine
              Propoxyphene
              Quetiapine
              Sertraline
              Temazepam
              Topiramate
              Tramadol
              Trazodone
              Valproic Acid
              Venlafaxine
              Verapamil
              Zolpidem
              Zopiclone
=== Associator model (full training set) ===

FPGrowth found 14 rules (displaying top 10)

 1. [Heroin=T, Cocaine=T]: 93 ==> [Fentanyl=T]: 63   conf:(0.68) <lift:(1.05)> lev:(0) conv:(1.06) 
 2. [Fentanyl=T]: 398 ==> [Heroin=T, Cocaine=T]: 63   conf:(0.16) <lift:(1.05)> lev:(0) conv:(1.01) 
 3. [Fentanyl=T]: 398 ==> [Heroin=T]: 201   conf:(0.51) <lift:(1.05)> lev:(0.01) conv:(1.04) 
 4. [Heroin=T]: 298 ==> [Fentanyl=T]: 201   conf:(0.67) <lift:(1.05)> lev:(0.01) conv:(1.08) 
 5. [Alcohol=T]: 140 ==> [Fentanyl=T]: 91   conf:(0.65) <lift:(1.01)> lev:(0) conv:(0.99) 
 6. [Fentanyl=T]: 398 ==> [Alcohol=T]: 91   conf:(0.23) <lift:(1.01)> lev:(0) conv:(1) 
 7. [Fentanyl=T]: 398 ==> [Cocaine=T]: 132   conf:(0.33) <lift:(1)> lev:(-0) conv:(1) 
 8. [Cocaine=T]: 205 ==> [Fentanyl=T]: 132   conf:(0.64) <lift:(1)> lev:(-0) conv:(0.98) 
 9. [Fentanyl=T, Cocaine=T]: 132 ==> [Heroin=T]: 63   conf:(0.48) <lift:(0.99)> lev:(-0) conv:(0.97) 
10. [Heroin=T]: 298 ==> [Fentanyl=T, Cocaine=T]: 63   conf:(0.21) <lift:(0.99)> lev:(-0) conv:(0.99) 


